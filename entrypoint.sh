#!/bin/ sh

set -e

# Updates the template with env vars and outputs the nginx config
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

nginx -g 'daemon off;'

